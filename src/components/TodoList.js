import React from 'react';
import './TodoList.css';


function TodoList({text, value, onDelete}) {
    return (
        <div className='todo-list'>
            <p>{text}</p>
            <button variant="outline-dark" value={value} onClick={onDelete}>Check</button>
        </div>
    )
}

export default TodoList;
