import React, { useState } from 'react';
import TodoList from './TodoList';
import uuid from 'react-uuid';
import TextField from '@material-ui/core/TextField';
import './TodoInput.css';

function TodoInput() {

    const [input,setInput] = useState('');
    const [list,setList] = useState([]);

    const addItem = (e)  => {
        e.preventDefault();
        setList([...list, {id : uuid(), todo : input }]);
        setInput('')
    }

    const handleDelete = (e) => {
        let id = e.target.value;
        let remainingList = list.filter(item => item.id !== id);
        setList(remainingList);
    }

    return (
        <div className="todo-box">
        <h1>To-Do list</h1>
        <form action="" onSubmit={addItem}>
           <TextField className="todo-input" fullWidth="true" id="standard-basic" label="Add Todo" type="text" value={input} onChange={(e)=> setInput(e.target.value)}/>
        </form>
        {list.map(item => <TodoList key={item.id} value={item.id} text={item.todo} onDelete={handleDelete}/>)}
        </div>
    )
}

export default TodoInput;
